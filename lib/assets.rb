require 'yaml'
require 'pp'
require 'fileutils'

module Assets
  CONFIG      = YAML.load_file('config.yml')
  NNAP        = File.expand_path(CONFIG['nnap'])
  ASSETS      = File.expand_path(CONFIG['assets'])
  OUTPUT      = File.expand_path(CONFIG['output'])
  ASSET_FILES = Dir.glob(File.join(ASSETS, '**/*'))

  MUSIC = []
  ASSET_FILES.map do |el|
    MUSIC << el if el.include?('music') && el.include?('.mp3')
  end

  PB_MM = []
  ASSET_FILES.map do |el|
    PB_MM << el if el.include?('pb_meatmarket') && el.include?('.mp3')
  end

  PB_STD = []
  ASSET_FILES.map do |el|
    PB_STD << el if el.include?('pb_standard') && el.include?('.mp3')
  end


  # Select one of these types:
  ## vala, valb, valb2, unit, cura, cure
  def asset_selector(vo_artist, asset_type, random = false)
    vo_code, vo_lang = vo_artist.split('_')
    assets = []
    ASSET_FILES.map do |el|
      assets << el if el.include?(asset_type) && el.include?('.mp3') && el.include?("#{vo_code}_") && el.include?(vo_lang)
    end
    return random_select(assets) if random.eql?(true)
    assets
  end

  def valb_select(vo_artist, value)
    vo_code, vo_lang = vo_artist.split('_')
    assets = []
    ASSET_FILES.map do |el|
      assets << el if el.include?('valb_') && el.include?('.mp3') && el.include?("#{vo_code}_") && el.include?(vo_lang) && el.include?(value)
    end
    assets[0]
  end
end

include Assets
