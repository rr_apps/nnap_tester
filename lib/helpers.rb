

def random_select(val)
  val[rand(val.size)]
end

def music_option_selector(music_option)
  case music_option
  when 1
    has_music = true
  when 2
    has_music = true if rand(2) == 1
  when 3
    has_music = false
  end
  return MUSIC[rand(MUSIC.size)] if has_music
  ''
end

def type_selector(type)
  FUNCTIONS[type - 1].tr(' ', '_')
end

def build_sp_path(options = {})
  voice     = options[:voice]
  pb        = File.basename(options[:pb], '.mp3')
  pr_a      = File.basename(options[:price_a], '.mp3')
  pr_b      = File.basename(options[:price_b], '.mp3')
  unit      = options[:unit_size] ||= ''
  unit      = File.basename(unit, '.mp3') unless unit.empty?
  curr      = options[:curr] ||= ''
  curr      = File.basename(curr, '.mp3') unless curr.empty?
  rndstr    = SecureRandom.hex(4)
  filename  = %(#{voice}_#{pb}_#{pr_a}_#{pr_b})
  filename  += %(_#{unit}) unless unit.empty?
  filename  += %(_#{curr}) unless curr.empty?
  filename  += %(_#{rndstr})
  date      =  Time.now.strftime('%Y-%m-%d')
  type      =  type_selector(options[:type])
  [File.join(OUTPUT, date, type), filename]
end

def hash_to_json_file(file, hash)
  FileUtils.mkpath(File.dirname(file)) unless File.exist?(File.dirname(file))
  File.open(file, 'w') do |f|
    f.puts JSON.pretty_generate(hash)
  end
end
