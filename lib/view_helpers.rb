
module ViewHelpers
  def line(len = 80)
    puts '#' * len
  end

  def side_line(len = 20)
    print '#' * len
  end

  def header(msg, len = 80)
    system('reset')
    msg_len = msg.size + 2
    sides = (len - msg_len) / 2
    line(len)
    print "#{side_line(sides)} "
    print "#{msg} "
    print side_line(sides).to_s
    puts
    line(len)
  end


  PB_TYPE = <<-EOF.split("\n")
Standard PB's - without unit size
Meat Market PB's - with unit size
  EOF

  def pb_type_selector
    print "\nPlease select the PB type:\n"
    num = 0
    PB_TYPE.each do |pb_opt|
      print %(#{num += 1}\) #{pb_opt}\n)
    end
    option = STDIN.gets.chomp
    exit_on_zero(option)
    if option == "1"
      return PB_STD, false
    else
      return PB_MM, true
    end
  end


  FUNCTIONS = <<-EOF.split("\n")
Type1 vala valb
Type2 vala valb with round robin
Type3 Type1 with rounded price currency ending
Type4 Type2 with currency
  EOF

  def function_selector
    puts "\nPlease select the function you wish to perform:"
    num = 0
    FUNCTIONS.each do |funct|
      print %(#{num += 1}\) #{funct}\n)
    end
    print "\nSelect 1-#{FUNCTIONS.size} or (0) To Exit "
    option = STDIN.gets.chomp
    exit_on_zero(option)
    option.to_i
  end

  def vo_selector
    puts "\nPlease select the voice you wish to test:"
    num = 0
    VO_ARTISTS.each do |vo|
      print %(#{num += 1}\) #{vo}\n)
    end
    print "\nSelect 1-#{VO_ARTISTS.size} or (0) To Exit "
    option = STDIN.gets.chomp
    exit_on_zero(option)
    VO_ARTISTS[option.to_i - 1]
  end

  MUSIC_OPTIONS = <<-EOF.split("\n")
Music under all adverts
Randomly select if the advert has music
No music under adverts
  EOF

  def music_selector
    print "\nPlease select the music option:\n"
    num = 0
    MUSIC_OPTIONS.each do |music_opt|
      print %(#{num += 1}\) #{music_opt}\n)
    end
    option = STDIN.gets.chomp
    exit_on_zero(option)
    option.to_i
  end

  def generate_sp?
    print "\nGenerate SP's for each JSON? (Y/n) "
    option = STDIN.gets.chomp
    option == 'Y' ? true : false
  end

  def sample_size(avail)
    print "\nThere are #{avail} PB's available.\nHow many examples would you like? "
    option = STDIN.gets.chomp
    exit_on_zero(option)
    option.to_i
  end

  def exit_on_zero(option)
    return unless option.to_i.zero?
    print 'Exiting...'
    exit(0)
  end
end

include ViewHelpers
