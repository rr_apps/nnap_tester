require 'yaml'
PROFILES = YAML.load_file('profiles.yml')

def profile_selector(vo_artist)
  voice_code, language = vo_artist.split('_')
  PROFILES['voice_profiles'].each do |obj|
    return obj if obj['voice_code'] == voice_code && obj['language'] == language
  end
  raise('Voice Artist Profile does not seem to exist! Please check profiles.yml')
end


