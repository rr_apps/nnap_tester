#!/usr/bin/env ruby

require 'securerandom'
require 'json'
require_relative 'lib/assets'
require_relative 'lib/view_helpers'
require_relative 'lib/helpers'
require_relative 'lib/profile_helper'

def compile_sp(type, vo_artist, music_option, pb_file, generate_sp)
  val_a = asset_selector(vo_artist, 'vala', true)
  unit_size = asset_selector(vo_artist, 'unit', true)
  profile = profile_selector(vo_artist)

  p profile
  val_b2 = ''
  currency_pad = ''
  currency = ''
  voice_code, lang_code = vo_artist.split('_')
  script = ''

  case type
  when 1 # Type1 vala valb
    val_b = asset_selector(vo_artist, 'valb', true)
  when 2 # Type2 vala valb with round robin
    val_b2 = asset_selector(vo_artist, 'valb2', true)
    val_b = valb_select(vo_artist, File.basename(val_b2))
  when 3 # Type3 Type1 with rounded price currency ending
    val_b = asset_selector(vo_artist, 'curre', true)
    currency_pad = profile['curr_pad']
  when 4 # Type4 Type2 with currency
    val_b2 = asset_selector(vo_artist, 'valb2', true)
    val_b = valb_select(vo_artist, File.basename(val_b2))
    currency = asset_selector(vo_artist, 'curra', true)
    currency_pad = profile['curr_pad']
  end

  sp_type = @unit_enable ? 'SPM' : 'SP'

  unit_pad = profile['unit_pad']
  unless @unit_enable
    unit_size, unit_pad  = ''
  end

  music = music_option_selector(music_option)
  out_path, sp_file = build_sp_path(type: type,
                                    voice: vo_artist,
                                    pb: pb_file,
                                    price_a: val_a,
                                    price_b: val_b)
  sp_path = File.join(out_path, sp_type, sp_file + '.mp3')
  json_path = File.join(out_path, 'JSON', sp_file + '.json')

  hash = {
    sp: sp_path,
    voice_code: voice_code,
    lang_code: lang_code,
    pb: pb_file,
    unit_size: unit_size,
    val_a1: val_a,
    val_b1: val_b,
    val_b2: val_b2,
    curr: currency,
    pad1: profile['pad_1'],
    pad2: profile['pad_2'],
    pad3: profile['pad_3'],
    curr_pad: currency_pad,
    unit_pad: unit_pad,
    music: music,
    script: script
  }
  hash_to_json_file(json_path, hash)
  line
  puts JSON.pretty_generate(hash)
  if generate_sp
    cmd = %(ruby #{CONFIG['nnap']} '#{json_path}')
    `#{cmd}`
  end
end

header('Radio Retail: NNAP Tester & Profiler')

pb_type, @unit_enable = pb_type_selector

vo_artists = []
pb_type.map do |el|
  vo_artists << el.split('/')[-2]
end
VO_ARTISTS = vo_artists.uniq

function = function_selector
vo_artist = vo_selector
music_option = music_selector
generate_sp = generate_sp?

vo_pbs = []
pb_type.map do |pb|
  vo_pbs << pb if pb.include?(vo_artist)
end

sample_size(vo_pbs.size).times do
  pb_file = vo_pbs[rand(vo_pbs.size)]
  compile_sp(function, vo_artist, music_option, pb_file, generate_sp)
end
